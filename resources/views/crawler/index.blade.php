@extends('layouts.default')

@section('content')


@if(Session::has('message'))

<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{ ucwords(Session::get('message')) }} 
</div>
@endif  



<!-- Contextual classes -->
<div class="panel panel-flat">
    <div class="panel-heading">
    <h5>Sigmalive List</h5>
    </div>

    <div class="panel-body">
        List of all the articles from sigmalive.com.


        <div class="table">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th><th>Title</th><th>Image</th><th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    @php $x=0; @endphp

                    @foreach($list as $item)

                        @php $x++; @endphp

                        <tr>
                            <td>{{ $x }}</td>
                            <td><a href="{{ $item->url }}" target="_blank">{{ $item->title }}</a></td>
                            <td><img src="{{ $item->image }}" width="100" /></td>
                            <td>{{ $item->date }}</td>                    
                        </tr>

                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
<!-- /contextual classes -->


@endsection
