@extends('layouts.default')

@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="{{ asset('layout/assets/bootstrap-switch-master/docs/css/highlight.css') }}">
<link rel="stylesheet" href="{{ asset('layout/assets/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.css') }}">
<style>tr.ttr{transition: ease-in .3s;} tr.disabled{opacity: .3}</style>
@endpush

@section('content')


@if(Session::has('message'))

<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{ ucwords(Session::get('message')) }} 
</div>
@endif  


<!-- Contextual classes -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Sections List <a href="{{ url('section/create') }}" class="btn btn-primary pull-right btn-sm">Add New Section</a></h5>
    </div>

    <div class="panel-body">
        List of all the sections. You can start drag to sort them


        <div class="table">
            <table class="table table-striped table-hover ">
                <thead>
                    <tr>
                        <th>ID</th><th>Title</th><th>Status</th><th>Last updated</th><th width="170px">Actions</th>
                    </tr>
                </thead>
                <tbody id="sorted_table" class="sorted_table connectedSortable">
                    @php $x=0; @endphp

                    @foreach($section as $item)

                    @php $x++; @endphp

                    <tr id="{{ $item->id }}" class="ttr @if(!$item->status) disabled @endif">
                        <td>{{ $item->id }}</td>
                        <td>
                            <a href="{{ url('section/' .  $item->id . '/edit') }}">{{ $item->title }}</a>
                        </td>
                        <td>{{ $status[$item->status] }}</td>
                        <td>{{ $item->updated_at }}</td>
                        <td>
                            <a href="{{ url('section/' . $item->id . '/edit') }}">
                                <button type="submit" class="btn btn-primary btn-xs">Update</button>
                            </a> 
                            /
                            <input class="switch-state" data-id="{{ $item->id }}" size="mini" type="checkbox" @if($item->status == 1) checked @endif>
                            /
                            {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['section', $item->id],

                            'style' => 'display:inline',
                            'name' => 'delete_' . $item->id,
                            'class' => 'deleteForm',
                            ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs delete',  'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete section', 'data-message' => 'Are you sure you want to delete this section ?', 'rel' => $item->id]) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>
<!-- /contextual classes -->


@include('layouts.deleteConfirm')

@endsection

@push('js')

<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('layout/assets/bootstrap-switch-master/dist/js/bootstrap-switch.js') }}"></script>
<script type="text/javascript">

    $(".sorted_table").sortable({update: function(event, ui){
        var data = $(this).sortable("toArray");

        $.ajax({
            data: {"sort": data, "_token": "{{ csrf_token() }}"},
            type: 'POST',
            url: "{{ route('section.sort') }}"
        });
    }});


    $(document).ready(function () {

        $('.switch-state').bootstrapSwitch();
        $('.switch-state').on('switchChange.bootstrapSwitch', function (event, state) {
         var here = $(this);
         var id = $(this).data('id');

         var start = $.post("{{ route('section.switch') }}", {state: state, id: id, '_token': $('input[name=_token]').val()});
         start.done(function (data) {
            if (data.status == "success") {
                if(!state){
                    here.closest("tr").addClass("disabled");
                }else{
                    here.closest("tr").removeClass("disabled");
                }
            } else {
                alert("Something went wrong, please try again");
            }
        });
     });

    });

</script>

@endpush