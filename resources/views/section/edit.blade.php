@extends('layouts.default')

@section('content')


<!-- Contextual classes -->
<div class="panel panel-flat">

    <div class="panel-heading">
        <h5>Edit section</h5>
    </div>

    <div class="panel-body">
        Editing the section data.

        <hr>

        @include('layouts._errors')

        {!! Form::model($section, [
            'method' => 'PATCH',
            'url' => ['section', $section->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

            <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-12">
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                {!! Form::label('type', ' Type: ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-12">
                    {!! Form::text('type', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('section_order') ? 'has-error' : ''}}">
                {!! Form::label('section_order', ' Section order (index): ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-12">
                    {!! Form::text('section_order', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('section_order', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
                {!! Form::label('content', ' Content: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-12">
                    {!! Form::textarea('content', $section->content, ['class' => 'form-control ckeditor']) !!}
                    {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', ' Status: ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-12">
                    {!! Form::select('status', $status, 1, ['class' => 'form-control']) !!}
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', ' Image: ', ['class' => 'col-sm-3 control-label ']) !!}
                <div class="col-sm-12">
                    <img width="50%" src="{{ $section->image_url }}" />
                </div>
            </div>

            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Replace Image: ', ['class' => 'col-sm-3 control-label ']) !!}
                <div class="col-sm-12">
                    <input name="image" accept="image/x-png,image/gif,image/jpeg" type="file" class="upload">
                    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    </div>
</div>
<!-- /contextual classes -->


@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('layout/assets/ckeditor/ckeditor.js') }}"></script>
@endpush