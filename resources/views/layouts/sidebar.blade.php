
<!-- Main sidebar -->
<div class="sidebar sidebar-main">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<a href="#" class="media-left"><img src="{{ asset('layout/assets/images/placeholder.jpg') }}" class="img-circle img-sm" alt=""></a>
					<div class="media-body">
						<span class="media-heading text-semibold">Mahmoud Mostafa</span>
						<div class="text-size-mini text-muted">
							<i class="icon-pin text-size-small"></i> &nbsp;Alexandria
						</div>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list">
							<li>
								<a href="#"><i class="icon-cog3"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
					<li><a href="#"><i class="icon-home4"></i> <span>Dashboard</span></a></li>

					<li>
						<a href="/user"><i class=" icon-users"></i> Users </a>
					</li>
					
					<li>
						<a href="/section"><i class=" icon-calendar"></i> Sections </a>
					</li>

					<li>
						<a href="{{ route('crawler') }}"><i class=" icon-map"></i> Mini Crawler </a>
					</li>

					@if(isset(Auth::user()->name))
					<li>
						<a href="/logout"><i class=" icon-exit"></i> Logout</a>
					</li>
					@endif


				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
			<!-- /main sidebar -->