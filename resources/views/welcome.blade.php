<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>Darkpony Task</title>

    <!-- CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('css/owl.transitions.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">

    <!--Icons-->
    <link rel="apple-touch-icon" sizes="114x114" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-120x120-precomposed.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-144x144-precomposed.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-152x152-precomposed.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-57x57-precomposed.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-60x60-precomposed.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="https://darkpony.eu/darkpony/data/icons/apple-touch-icon-76x76-precomposed.png" />
    <link rel="icon" sizes="160x160" type="image/png" href="https://darkpony.eu/darkpony/data/icons/favicon-160x160.png" />
    <link rel="icon" sizes="16x16" type="image/png" href="https://darkpony.eu/darkpony/data/icons/favicon-16x16.png" />
    <link rel="icon" sizes="32x32" type="image/png" href="https://darkpony.eu/darkpony/data/icons/favicon-32x32.png" />
    <link rel="icon" sizes="96x96" type="image/png" href="https://darkpony.eu/darkpony/data/icons/favicon-96x96.png" />
    <meta name="msapplication-square144x144logo" content="https://darkpony.eu/darkpony/data/icons/mstile-144x144.png" />
    <meta name="msapplication-square150x150logo" content="https://darkpony.eu/darkpony/data/icons/mstile-150x150.png" />
    <meta name="msapplication-square310x310logo" content="https://darkpony.eu/darkpony/data/icons/mstile-310x310.png" />
    <meta name="msapplication-TileImage" content="https://darkpony.eu/darkpony/data/icons/mstile-70x70.png" />
    <meta name="msapplication-square70x70logo" content="https://darkpony.eu/darkpony/data/icons/mstile-70x70.png" />
    
</head><!--/head-->

<body id="home" class="homepage">
    <!-- #e2b02a -->

    <div class="up"><i class="fa fa-chevron-up fa-lg"></i></div>

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="col-2">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt="logo"></a>
                    </div>
                </div>
                <div class="col-10 right-menu"> 
                    <div class="top-top-header">
                        <div class="row">
                            <div class="col-sm-11 email">info@company.com</div>
                            <div class="col-sm-1">
                                <a class="lang active" href="javascript: void(0);">EN</a>
                                <a class="lang" href="javascript: void(0);">GR</a>
                            </div>
                        </div>
                    </div>
                    <div class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li class="scroll active"><a href="#home">Home</a></li>
                            <li class="scroll"><a href="#about">About Us</a></li>
                            <li class="scroll"><a href="#products">Products</a></li>
                            <li class="scroll"><a href="#services">Service</a></li>

                            <li class="scroll"><a href="#contactz">Contact</a></li>                        
                            <li class="search"><a href="#search"><i class="fa fa-search fa-lg"></i></a></li>                        
                        </ul>
                    </div>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->

    <section id="main-slider">
        <div class="owl-carousel">
            <div class="item" style="background-image: url(images/slider/image1.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2><span>Nemo</span> enim ipsam voluptatem quia voluptas</h2>
                                    <p> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut. </p>
                                    <!-- <a class="btn btn-primary btn-lg" href="#">Read More</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
            <div class="item" style="background-image: url(images/slider/image1.png);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Cum soluta nobis <span>TEMPORE</span></h2>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore. </p>
                                    <!-- <a class="btn btn-primary btn-lg" href="#">Read More</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
        </div><!--/.owl-carousel-->
    </section><!--/#main-slider-->

    

    <section id="about">
        <div class="container">
            <div class="row about-image">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 paragraph">
                            <h1>About us</h1>
                            <h3>faucibus dolor gravida eget. Quisque ac dapibus nunc. Interdum et malesuada</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at lacinia augue, posuere ullamcorper erat. Nullam in laoreet orci. Nulla eget fermentum lectus, quis elementum enim. Morbi in metus vehicula, iaculis augue et, eleifend est. Proin elementum enim vitae blandit varius. Suspendisse sodales est in nisi molestie, eget fermentum ex condimentum. Cras pretium vitae metus elementum finibus. Aenean et magna sed enim fermentum dignissim ut ac diam. Pellentesque gravida odio tortor, et fringilla nisi pretium et. Etiam fringilla eget odio ut eleifend. Cras dictum vehicula orci, a iaculis tortor elementum eget. Ut nisi ipsum, laoreet vel augue ac, viverra ornare urna.
                                <br>
                                <br>
                                Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit. Ut ullamcorper, neque sit amet molestie interdum, magna mauris fermentum elit, eget pharetra dolor turpis sed mauris. Nam in tristique ipsum. Morbi eget mollis neque. Nunc hendrerit ut lorem ut convallis. Nullam bibendum risus a iaculis egestas. Curabitur at dui ligula. Nullam id est ut sapien convallis rutrum. Nam aliquet rhoncus velit, non fermentum mauris. Duis sed arcu nec urna vulputate iaculis non vitae risus.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section><!--/#about-->


        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Services</h1>
                        <div class="seeder">
                            <img src="./images/services/icon1.svg" />
                            <span class="seeds"></span>
                            <img src="./images/services/icon2.svg" />
                        </div>
                    </div>
                </div>

                <div class="row services-row">
                    <div class="col-sm-4">
                        <p>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit.</p>
                        <p>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit. Ut ullamcorper, neque sit amet molestie interdum, magna mauris fermentum elit, eget pharetra dolor turpis sed mauris. Nam in tristique ipsum. Morbi eget mollis neque.</p>
                        <p>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit. Ut ullamcorper, neque sit amet molestie interdum, magna mauris fermentum elit, eget pharetra dolor turpis sed mauris. Nam in tristique ipsum. Morbi eget mollis neque.</p>
                    </div>
                    <div class="col-sm-4">
                        <ul>
                            <li><span>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit.</span></li>
                            <li><span>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit.</span></li>
                            <li><span>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit.</span></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul>
                            <li><span>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit.</span></li>
                            <li><span>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit.</span></li>
                            <li><span>Vivamus nibh mi, sodales vitae lacinia pharetra, congue sit amet nulla. Proin molestie blandit quam a tincidunt. Suspendisse potenti. Duis sed molestie metus. Sed id eros a nibh maximus blandit.</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section><!--/#services-->


        <section id="products">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Products</h1>
                        <div class="products-list">
                            <div class="row">
                                <div class="col-sm-4 outline main-des">
                                    <div class="product ">
                                        <div class="title"></div>
                                        <div class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at lacinia augue, posuere ullamcorper erat. Nullam in laoreet orci. Nulla eget fermentum lectus, quis elementum enim. Morbi in metus vehicula, iaculis augue et, eleifend est. Proin elementum enim vitae blandit varius. Suspendisse sodales est in nisi molestie, eget fermentum ex condimentum. Cras pretium vitae metus elementum finibus</div>
                                    </div>
                                </div>
                                <div class="col-sm-4 outline">
                                    <div class="product" style="background: url('./images/products/wheat.jpg');">
                                        <div class="move">
                                            <div class="title">Wheat</div>
                                            <div class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at lacinia augue, posuere ullamcorper erat. Nullam in laoreet orci. Nulla eget fermentum lectus, quis elementum enim. Morbi in metus vehicula, iaculis augue et, eleifend est. Proin elementum enim vitae blandit varius. Suspendisse sodales est in nisi molestie, eget fermentum ex condimentum. Cras pretium vitae metus elementum finibus</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 outline">
                                    <div class="product" style="background: url('./images/products/barley.jpg');">
                                        <div class="move">
                                            <div class="title">Barley</div>
                                            <div class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at lacinia augue, posuere ullamcorper erat. Nullam in laoreet orci. Nulla eget fermentum lectus, quis elementum enim. Morbi in metus vehicula, iaculis augue et, eleifend est. Proin elementum enim vitae blandit varius. Suspendisse sodales est in nisi molestie, eget fermentum ex condimentum. Cras pretium vitae metus elementum finibus</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 outline">
                                    <div class="product" style="background: url('./images/products/rye.png');">
                                        <div class="move">
                                            <div class="title">Rye</div>
                                            <div class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at lacinia augue, posuere ullamcorper erat. Nullam in laoreet orci. Nulla eget fermentum lectus, quis elementum enim. Morbi in metus vehicula, iaculis augue et, eleifend est. Proin elementum enim vitae blandit varius. Suspendisse sodales est in nisi molestie, eget fermentum ex condimentum. Cras pretium vitae metus elementum finibus</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 outline">
                                    <div class="product" style="background: url('./images/products/beans.jpg');">
                                        <div class="move">
                                            <div class="title">Beans</div>
                                            <div class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at lacinia augue, posuere ullamcorper erat. Nullam in laoreet orci. Nulla eget fermentum lectus, quis elementum enim. Morbi in metus vehicula, iaculis augue et, eleifend est. Proin elementum enim vitae blandit varius. Suspendisse sodales est in nisi molestie, eget fermentum ex condimentum. Cras pretium vitae metus elementum finibus</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 outline">
                                    <div class="product" style="background: url('./images/products/peas.jpg');">
                                        <div class="move">
                                            <div class="title">Peas</div>
                                            <div class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at lacinia augue, posuere ullamcorper erat. Nullam in laoreet orci. Nulla eget fermentum lectus, quis elementum enim. Morbi in metus vehicula, iaculis augue et, eleifend est. Proin elementum enim vitae blandit varius. Suspendisse sodales est in nisi molestie, eget fermentum ex condimentum. Cras pretium vitae metus elementum finibus</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </section><!--/#products-->

        <!--partners-->
        <section id="partners">
            <div class="container">
                <div class="row">
                    <div id="owl-partners" class="owl-carousel">
                        <div><img src="./images/logo.png" alt=""></div>
                        <div><img src="./images/logo.png" alt=""></div>
                        <div><img src="./images/logo.png" alt=""></div>
                        <div><img src="./images/logo.png" alt=""></div>
                        <div><img src="./images/logo.png" alt=""></div>

                    </div>
                </div>
            </div>
        </section><!--/#partners-->


        <section id="contactz">
            <div class="split left">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="box">
                            <h1>Contact Us</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                            <p><i class="fa fa-map-marker"></i>Nullamo aucto 24, country.</p>
                            <p><i class="fa fa-phone"></i>00 99 123 321</p>
                            <p><i class="fa fa-fax"></i>0099123321.</p>
                            <p><i class="fa fa-envelope"></i>info@company.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="split right">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="box">
                        <h1>Get in touch</h1>
                        <form action="">

                          <div class="input-container">
                            <input class="input-field" type="text" placeholder="Username" name="usrnm">
                            <i class="fa fa-user icon"></i>
                        </div>

                        <div class="input-container">
                            <input class="input-field" type="text" placeholder="Email" name="email">
                            <i class="fa fa-envelope icon"></i>
                        </div>

                        <div class="input-container">
                            <textarea class="input-field" placeholder="Your message" name="message"></textarea>
                            <i class="fa fa-comment icon"></i>
                        </div>

                        <button type="button" class="btn">Send a message</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section><!--/#contactz-->



<section id="contact">
    <div id="google-map" style="height:450px" data-latitude="35.1740385" data-longitude="33.3463873"></div>
</section><!--/#bottom-->

<section id="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-4"><img src="./images/logo.png" /></div>
            <div class="col-lg-6 col-sm-6">
                <div class="subscribe">
                    <div class="title">receive our newsletter</div>
                    <div class="body">Lorem ipsum dolor sit amet, consectetur adipiscing elit</div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <input class="input-field" type="text" placeholder="Email" name="email">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <button class="btn" type="button">Subscribe</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-hidden col-sm-2"></div>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                &copy; 2019 Company all rights reserved| Designed & Developed by Mahmoud Mostafa.
            </div>

        </div>
    </div>
</footer><!--/#footer-->


<script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-4.3.1.min.js') }}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/mousescroll.js') }}"></script>
<script src="{{ asset('js/smoothscroll.js') }}"></script>

<script src="{{ asset('js/main.js') }}"></script>

</body>
</html>