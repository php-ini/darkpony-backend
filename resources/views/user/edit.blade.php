@extends('layouts.default')

@section('content')


<!-- Contextual classes -->
<div class="panel panel-flat">

    <div class="panel-heading">
        <h5>Edit User</h5>
    </div>

    <div class="panel-body">
        Editing the user data.

        <hr>

        @include('layouts._errors')

        {!! Form::model($user, [
            'method' => 'PATCH',
            'url' => ['user', $user->id],
            'class' => 'form-horizontal'
        ]) !!}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Full Name: ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                {!! Form::label('username', ' User Name: ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-6">
                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', ' Email: ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                {!! Form::label('password', ' Password: ', ['class' => 'col-sm-3 control-label  fa fa-flash']) !!}
                <div class="col-sm-6">
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', ' Status: ', ['class' => 'col-sm-3 control-label fa  fa-flash']) !!}
                <div class="col-sm-6">
                    {!! Form::select('status', $status, $user->status, ['class' => 'form-control']) !!}
                    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    </div>
</div>
<!-- /contextual classes -->


@endsection
