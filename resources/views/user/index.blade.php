@extends('layouts.default')

@section('content')


@if(Session::has('message'))

<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{ ucwords(Session::get('message')) }} 
</div>
@endif  



<!-- Contextual classes -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5>Users List <a href="{{ url('user/create') }}" class="btn btn-primary pull-right btn-sm">Add New User</a></h5>
    </div>

    <div class="panel-body">
        List of all the users that are using daropony.
    

    <div class="table">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th><th>Name</th><th>Username</th><th>Email</th><th>Role</th><th>Status</th><th>Last Logged</th><th width="170px">Actions</th>
                </tr>
            </thead>
            <tbody>
            @php $x=0; @endphp

                @foreach($user as $item)

                @php $x++; @endphp

                <tr @if(Auth::user()->email == $item->email) style="background: #c6ffb3;" @endif>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td><a href="{{ url('user/' .  $item->id . '/edit') }}">{{ $item->username }}</a></td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->role }}</td>
                    <td>{{ $status[$item->status] }}</td>
                    <td>{{ $item->last_logged }}</td>
                    <td>
                        <a href="{{ url('user/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> 
                        
                        @if(Auth::user()->email != $item->email)
                        /
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['user', $item->id],
                                
                                'style' => 'display:inline',
                                'name' => 'delete_' . $item->id,
                                'class' => 'deleteForm',
                            ]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs delete',  'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete user', 'data-message' => 'Are you sure you want to delete this user ?', 'rel' => $item->id]) !!}
                            {!! Form::close() !!}

                         @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $user->render() !!} </div>
    </div>
</div>
</div>
<!-- /contextual classes -->


@include('layouts.deleteConfirm')


@endsection
