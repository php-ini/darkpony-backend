<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () { // Authenticated Users Only
	// users resource
	Route::resource('user', 'userController');
	// sections resources
	Route::resource('section', 'sectionController');
	// route for re-sorting the sections grid
	Route::post('section/sort', ['uses' => 'sectionController@sortSections', 'as' => 'section.sort']);
	// route to toogle section status
	Route::post('section/switch', ['uses' => 'sectionController@switchSections', 'as' => 'section.switch']);

	Route::get('/crawler', 'crawlerController@index')->name('crawler');

});

// Authentication routes...
Route::get('/logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'logout']);

