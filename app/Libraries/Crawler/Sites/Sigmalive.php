<?php
namespace App\Libraries\Crawler\Sites;

use App\Libraries\Crawler\Crawler;
use App\Libraries\Crawler\Sites\SitesInterface;
/**
 * Class Sigmalive
 * Class for parsing the website sigmalive.com
 *
 * @author Mahmoud Mostaf <jinkazama_m@yahoo.com>
 */

class Sigmalive implements SitesInterface
{
	public static function getURL()
	{
		return 'https://www.sigmalive.com/news/local';
	}

	public static function getPattern()
	{
		return '~<div class="article--column">\s*<div class="article--img">\s*<a\s*href="([^"]+)"><img src="([^"]+)"[^>]+></a>\s*</div>\s*<div class="article--grid--infos">\s*<a\s*href="([^"]+)" class="article--title"><span>([^<]+)</span></a>\s*<div class="time--and-cat">\s*<span><time class="time" datetime="([0-9\- ]+)">[0-9\. ]+</time>~is';
	}
	
	public static function parseContents($html)
	{
		preg_match_all(self::getPattern(), $html, $matches);

		$list = [];

		foreach($matches[1] as $k => $v) {
			$list[$k]['url'] = $v;
			$list[$k]['image'] = 'https://www.sigmalive.com' . $matches[2][$k];
			$list[$k]['title'] = $matches[4][$k];
			$list[$k]['date'] = $matches[5][$k];
		}

		return json_decode(json_encode($list));
	}
}