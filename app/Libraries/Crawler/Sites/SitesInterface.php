<?php
namespace App\Libraries\Crawler\Sites;
/** 
 *	Interface SitesInterface
 *	Contract for the sites classes.
 *
 *	@author Mahmoud Mostafa <jinkazama_m@yahoo.com>
 */

interface SitesInterface
{

	public static function getURL();

	public static function getPattern();
	
	public static function parseContents($html);

}