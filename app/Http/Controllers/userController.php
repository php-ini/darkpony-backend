<?php
/**
 * User Class
 * To manage All transactions related to the registered users in the application.
 * 
 * @author Mahmoud Mostafa <jinkazama_m@yahoo.com>
 * @version 1.0
 */
namespace App\Http\Controllers;

use DB;
use Hash;
use Auth;
use Session;
use App\User;
use Redirect;
use Validator;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class userController extends Controller
{
    private $roles = [];

    public function __construct()
    {

        view()->share('currentController', 'Users');

    }

    /**
     * List Suppliers
     * Display a listing of the Suppliers from the database table "user" with pagination.
     *
     * @return Response
     */
    public function index()
    {
        $user = user::paginate(15);
        
        $status = [0 => 'Inactive', 1 => 'Active'];

        return view('user.index', ['user' => $user, 'status' => $status]);
    }

    /**
     * Create New Supplier Form
     * Show the HTML form for creating a new Supplier in the database table "user".
     *
     * @return Response
     */
    public function create()
    {

       $status[0] = 'Inactive';
       $status[1] = 'Active';

       return view('user.create', ['status' => $status]);
   }

    /**
     * Insert New Supplier
     * Actual Store a newly created user in database table "user".
     *
     * @return Response
     */
    public function store(Request $request)
    {

        if($this->is_valid($request , $v) === false){
            return redirect('user/create')
            ->withErrors($v)
            ->withInput();
        }
        
        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        user::create($input);

        Session::flash('message', 'user added successfully !');

        return redirect('user');
    }

    /**
     * Show Supplier Data
     * Display the specified Supplier and all of his information.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = user::findOrFail($id);

        $status[0] = 'Inactive';
        $status[1] = 'Active';

        return view('user.show', ['user' => $user, 'status' => $status]);
    }

    /**
     * Edit One Supplier
     * Show the HTML form for editing the specified Supplier.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = user::findOrFail($id);

        $status[0] = 'Inactive';
        $status[1] = 'Active';

        return view('user.edit', ['user' => $user, 'status' => $status]);
    }

    /**
     * Update Supplier Data
     * Actual Update the specified Supplier Data in database table "user".
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        if($this->is_valid($request , $v, $id) === false){
            return redirect('user/'. $id .'/edit')
            ->withErrors($v)
            ->withInput();
        }
        
        $user = user::findOrFail($id);

        $input = $request->all();

        if ($input['password'] == '') {
            unset($input['password']);
        } else {
            $input['password'] = bcrypt($input['password']);
        }

        $user->update($input);

        Session::flash('message', 'user updated successfully !');

        return redirect('user');
    }

    /**
     * Delete One Supplier Row
     * Remove the specified Row Supplier from the Database table "user".
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {

        if(Auth::user()->id == $id){
            return redirect('user')->withErrors(['email' => 'You cannot delete the active user']);
        }

        user::destroy($id);

        Session::flash('message', 'user deleted!');

        return redirect('user');
    }
    
    /**
     * Validate Parameters
     * Actual Validate The Incoming Request Parameters.
     *
     * @return Boolean
     */
    public function is_valid($request , &$v, $id=null){

        $input = $request->all();
        
        if(isset($input['_method']) && $input['_method'] == 'PATCH'){

            $v = Validator::make($request->all(), [
                'email' => 'required|email|min:2|unique:user,email,' . $id,
                'username' => 'required|string|min:2',
                'name' => 'required|string|min:2',
                // 'password' => 'required|string|min:2',
                'status' => 'required'
                ]);

        }else{

            $v = Validator::make($request->all(), [
                'email' => 'required|email|min:2|unique:user',
                'username' => 'required|string|min:2',
                'name' => 'required|string|min:2',
                'password' => 'required|string|min:2',
                'status' => 'required',
                ]);
        }

        if ($v->fails()) {
            return false;
        }else { 
            return true; 
        }
    }


}