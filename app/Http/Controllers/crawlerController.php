<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\Crawler\Crawler;
use App\Libraries\Crawler\Sites\Sigmalive;

class crawlerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Crawl website.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $url = Sigmalive::getURL();

        $contents = Crawler::getCurl($url);

        $list = Sigmalive::parseContents($contents);
        
        return view('crawler.index', compact('list'));
    }
}
