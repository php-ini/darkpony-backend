<?php
namespace App\Http\Controllers;

use File;
use Session;
use Validator;
use App\section;
use Illuminate\Http\Request;

/**
 * Class sectionController
 * Section controller class to manage the resources of section routes
 *
 * @author Mahmoud Mostaf <jinkazama_m@yahoo.com>
 */

class sectionController extends Controller
{
    private $status;
    
    private $uploadTo;

    public function __construct()
    {

        view()->share('currentController', 'Sections');
        
        $this->status = ['Inactive', 'Active'];

        $this->uploadTo = 'uploads';
    }

    /**
     * List Suppliers
     * Display a listing of the Suppliers from the database table "section" with pagination.
     *
     * @return Response
     */
    public function index()
    {
        $section = section::orderBy('section_order')->get();
        
        return view('section.index', ['section' => $section, 'status' => $this->status]);
    }

    /**
     * Create New Section Form
     * Show the HTML form for creating a new Section in the database table "section".
     *
     * @return Response
     */
    public function create()
    {

       return view('section.create', ['status' => $this->status]);
   }

    /**
     * Insert New Section
     * Actual Store a newly created section in database table "section".
     *
     * @return Response
     */
    public function store(Request $request)
    {

        if($this->is_valid($request , $v) === false){
            return redirect('section/create')
            ->withErrors($v)
            ->withInput();
        }
        
        $input = $request->all();

        if (request()->hasFile('image')) {
            $image = request()->file('image');

            $name = time().'.'.$image->getClientOriginalExtension();
            $directory = $this->uploadTo;
            
            $destinationPath = public_path($directory);
            $image->move($destinationPath, $name);
            
            $input['image'] = $directory.'/'.$name;
        }

        section::create($input);

        Session::flash('message', 'Section added successfully !');

        return redirect('section');
    }

    /**
     * Show Section Data
     * Display the specified Section and all of his information.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $section = section::findOrFail($id);

        return view('section.show', ['section' => $section, 'status' => $this->status]);
    }

    /**
     * Edit One section
     * Show the HTML form for editing the specified section.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $section = section::findOrFail($id);

        return view('section.edit', ['section' => $section, 'status' => $this->status]);
    }

    /**
     * Update section Data
     * Actual Update the specified section Data in database table "section".
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        if($this->is_valid($request , $v, $id) === false){
            return redirect('section/'. $id .'/edit')
            ->withErrors($v)
            ->withInput();
        }
        
        $section = section::findOrFail($id);

        $input = $request->all();

        if (request()->hasFile('image')) {

            $directory = $this->uploadTo;

            $old_full_image = public_path($directory.'/'.$section->image);

            if (file_exists($old_full_image)) {
                File::delete($old_full_image);
            }

            $image = request()->file('image');

            $name = time().'.'.$image->getClientOriginalExtension();
            
            $destinationPath = public_path($directory);
            $image->move($destinationPath, $name);
            
            $input['image'] = $directory.'/'.$name;
        }

        $section->update($input);

        Session::flash('message', 'Section updated successfully !');

        return redirect('section');
    }

    /**
     * Delete One section Row
     * Remove the specified Row section from the Database table "section".
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        section::destroy($id);

        Session::flash('message', 'section deleted!');

        return redirect('section');
    }
    
    /**
     * Validate Parameters
     * Actual Validate The Incoming Request Parameters.
     *
     * @return Boolean
     */
    public function is_valid($request , &$v, $id=null)
    {
        $input = $request->all();
        
        if(isset($input['_method']) && $input['_method'] == 'PATCH'){

            $v = Validator::make($request->all(), [
                'title' => 'required|string|min:2',
                'type' => 'required|string|min:2|unique:section,type,' . $id,
                'content' => 'required|string|min:2',
                'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required'
                ]);

        } else {

            $v = Validator::make($request->all(), [
                'title' => 'required|string|min:2',
                'type' => 'required|string|min:2|unique:section',
                'content' => 'required|string|min:2',
                'image' => 'sometimes|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'status' => 'required'
                ]);
        }

        return $v->fails() ? false : true;
        
    }

    /**
     * Sort sections
     * Sort the selected section from the grid by id
     *
     * @return Response JSON
     */
    public function sortSections()
    {

        $input = request()->all();

        $saved = 0;

        foreach ($input['sort'] as $key => $id) {

            $section = section::findOrFail($id);
            $section->section_order = $key;

            if ($section->save()) {
                $saved++;
            }
        }

        if ($saved == count($input['sort'])) {
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'failed']);
        }
    }

    /**
     * Toggle sections status
     * Toggle sections grid to switch status
     *
     * @return Response JSON
     */
    public function switchSections()
    {
        $input = request()->all();
        
        $id = $input['id'];
        
        $state = $input['state'] ? 1 : 0;
        
        $section = section::findOrFail($id);
        
        $section->status = $state;

        if ($section->save()) {
             return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'failed']);
        }
        
    }
    
}
